==================================================================
https://keybase.io/john_nelson
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://john-nelson.gitlab.io
  * I am john_nelson (https://keybase.io/john_nelson) on keybase.
  * I have a public key ASDLUWnGnqKGS5mRCtlawoSOlIIFPaywpjCtcl5k2KnMwQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120cb5169c69ea2864b99910ad95ac2848e9482053dacb0a630ad725e64d8a9ccc10a",
      "host": "keybase.io",
      "kid": "0120cb5169c69ea2864b99910ad95ac2848e9482053dacb0a630ad725e64d8a9ccc10a",
      "uid": "46bbb735d266ef0162e21f9cba22fe19",
      "username": "john_nelson"
    },
    "merkle_root": {
      "ctime": 1532573224,
      "hash": "f059de9233080d26eddb4895b79df6f5eec6f6919080be9deb4989510faf365b40083aa92f6415c541db1b6ebe5cfeee66d7964f1bcb6f131726bc93b2344be9",
      "hash_meta": "f55c814430c1d7ea6053e52c50d0c68123370bc78622293fc1d0d6a0117708a9",
      "seqno": 3332438
    },
    "service": {
      "entropy": "hOtnBUjnpqt3fG2coTX+ip5m",
      "hostname": "john-nelson.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.4.0"
  },
  "ctime": 1532573300,
  "expire_in": 504576000,
  "prev": "23eae88faf5cb8181ef68fe3e218c6b2ce478a9b93dd1624b250dfc38052a513",
  "seqno": 7,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgy1Fpxp6ihkuZkQrZWsKEjpSCBT2ssKYwrXJeZNipzMEKp3BheWxvYWTESpcCB8QgI+roj69cuBge9o/j4hjGss5HipuT3RYkslDfw4BSpRPEINRq2PeGxRvDCTsPuV+moXe+xZqvbU5dWKuzyOlYjoV8AgHCo3NpZ8RAGFC3CHKJd2yZrXeHg1JbQXUlE0wSE0T7Dy2kd3QyOKKvGufPIu0yJ3ZCckt0T+IxxW1ZCEELAKmH10fWdOJkCqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIATTRdVxvaD8FH+0tAo3JA7XR8S08EBOUBg9DeqYYZdNo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/john_nelson

==================================================================